+++
title = "Gender Speech"
+++

## Description

Gender Speech in radio & TV.

## Accéder au code

[![Binder](https://mybinder.org/static/images/badge_logo.svg)](https://mybinder.org/v2/gl/jibe-b%2Fmentorat-women-speech/master?urlpath=lab%2Ftree%2Fnotebooks%2Findex.ipynb)

## Contribuer

[Ouvrez un ticket](../tutoriels/ouvrir-un-ticket)
