---
title: HYCAR
---

## Description

HYCAR

## Accéder au code

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jibe-b%2Fhycar-analyse-de-carnets-de-blog/master?urlpath=lab%2Ftree%2Fmain.ipynb)

## Faire remonter une suggestion

[ouvrir un ticket](https://gitlab.com/jibe-b/hycar-analyse-de-carnets-de-blog/issues/new)